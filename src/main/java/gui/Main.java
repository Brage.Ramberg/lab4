package gui;

import cellular.BriansBrain;
import cellular.CellAutomaton;
import cellular.GameOfLife;

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new BriansBrain(500,500);
		//CellAutomaton ca = new BriansBrain(100, 100);
		CellAutomataGUI.run(ca);
	}
	

}
