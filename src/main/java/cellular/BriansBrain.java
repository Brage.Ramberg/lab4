package cellular;

import java.util.Random;
import datastructure.IGrid;
import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;
	

	int rows;
	int cols; 

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
			for (int i = 0; i < numberOfRows(); i++){
				for (int j = 0; j < numberOfColumns(); j++) {
					CellState newCellState = getNextCell(i, j);
					nextGeneration.set(i, j, newCellState);
				}				
			}
			currentGeneration = nextGeneration;
	}

	
	@Override
	public CellState getNextCell(int row, int col) {
		int countAlive = countNeighbors(row, col, CellState.ALIVE);
		boolean alive = getCellState(row, col) == CellState.ALIVE;
		boolean dead = getCellState(row, col) == CellState.DEAD;
        boolean dying = getCellState(row, col) == CellState.DYING;

		//living cell with less than 2 neighbours die	
		if (alive){
			return CellState.DYING;
		}
		//living cell with 2 or 3 neighbours live
		else if (dying){
			return CellState.DEAD;
		}
		//living cell with more than 3 neigbhours die
		else if (dead && countAlive == 2){
			return CellState.ALIVE;
		}
		
		else{
			return getCellState(row, col);
		}
	}
	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int neighbours = 0;
	// 	//if (0,0) top left
	// 	if (row == 0 && col == 0){
	// 		if(getCellState(row, col+1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row+1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row+1, col+1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}

	// 	//if (0,max) top right
		
	// 	else if (row == 0 && col == numberOfColumns()){
	// 		if(getCellState(row, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row+1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row+1, col-1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}
	// 	// if (max, 0) left bottom
	// 	else if (row == numberOfRows() && col == 0){
	// 		if(getCellState(row+1, col) == state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row+1, col+1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row+1, col-1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}
	// 	// if (max,max)
	// 	else if (row == numberOfRows() && col == numberOfColumns()){
	// 		if(getCellState(row+1, col) == state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if(getCellState(row-1, col-1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}


	// 	//if starting point is at the top
	// 	else if (row == 0){
	// 		if (getCellState(row, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row, col+1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col+1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}
	// 	//if starting point at bottom
	// 	else if (col == numberOfRows()){
	// 		if (getCellState(row, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row, col+1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col+1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}
	// 	//starting point left side
	// 	else if (col == 0){
	// 		if (getCellState(row-1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col+1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}
	// 	//starting point right side
	// 	else if (col == numberOfColumns()){
	// 		if (getCellState(row-1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col+1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}
	// 	//if it has 8 neighbours
	// 	else {	
	// 		if (getCellState(row-1, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row-1, col+1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row, col+1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col-1)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col)== state){
	// 			neighbours += 1;
	// 		}
	// 		if (getCellState(row+1, col+1)== state){
	// 			neighbours += 1;
	// 		}
		
	// 	}	
	// return neighbours;	
	// }	
		// }
		
		for (int i = row-1; i < row+2; i++){
			for (int j = col-1; j < col+2; j++) {
				if (i < 0 || i >= currentGeneration.numRows() || j < 0 || j >= currentGeneration.numColumns()){
					continue;

				}
				else if (i == row && j == col){
					continue;
				}
				else {
					if (state == CellState.DEAD){
						if (currentGeneration.get(i, j) != state){
							neighbours +=1;
						}	
						}
					else if (state == CellState.ALIVE){
						if (currentGeneration.get(i, j) == state){
							neighbours +=1;
						}
					}
				}
			}	
			}
			return neighbours;							
		}
			
		
	
		
	
	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}

    
}
