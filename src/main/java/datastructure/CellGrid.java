package datastructure;

import java.util.ArrayList;

import javax.lang.model.element.Element;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] cellstate;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols =  columns;
        cellstate = new CellState [rows][cols];

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++) {
                cellstate[i][j] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    // Set the contents of the cell in the given row,column location.
	
    @Override
    public void set(int row, int column, CellState element) {
        
        
        if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns())) {     
            cellstate[row][column] = element;  
        }
        else {
            throw new IndexOutOfBoundsException("skrt");  
        }
    }
	// Get the contents of the cell in the given row,column location.
	
    @Override
    public CellState get(int row, int column) {
        
        if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns())) {
            return cellstate[row][column];
        }
        
        else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        IGrid gridcopy = new CellGrid(this.rows, this.cols, CellState.ALIVE);
        
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++) {
                gridcopy.set(i, j, cellstate[i][j]);
            }
        }
        return gridcopy;
    }
}
